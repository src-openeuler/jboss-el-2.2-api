%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}
Name:                jboss-el-2.2-api
Version:             1.0.2
Release:             2
Summary:             Expression Language 2.2 API
License:             CDDL-1.0 or GPL-2.0-with-classpath-exception
URL:                 http://www.jboss.org
BuildArch:           noarch
Source0:             https://github.com/jboss/jboss-el-api_spec/archive/%{namedversion}.tar.gz
BuildRequires:       maven-local mvn(org.apache.felix:maven-bundle-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-source-plugin)
BuildRequires:       mvn(org.jboss:jboss-parent:pom:)
%description
Expression Language 2.2 API classes.

%package javadoc
Summary:             Javadocs for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n jboss-el-api_spec-%{namedversion}

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license LICENSE
%doc README

%files javadoc -f .mfiles-javadoc
%license LICENSE

%changelog
* Wed May 11 2022 liyanan <liyanan32@h-partners.com> - 1.0.2-2
- License compliance rectification

* Mon Nov 2 2020 huanghaitao <huanghaitao8@huawei.com> - 1.0.2-1
- package init
